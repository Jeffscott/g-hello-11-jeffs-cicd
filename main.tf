terraform {
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.0"
        }
    }
    backend "http" {
    }
}

provider "aws" {
  region = "us-east-2"
}
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}
resource "aws_s3_bucket" "b" {
  bucket = "jeff-bucket3"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
    
  }
}

resource "aws_s3_object" "object" {
  bucket = "${aws_s3_bucket.b.id}"
  key    = "g-hello-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-0.0.1-SNAPSHOT.jar"
  acl  = "public-read-write"
  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"

  ingress {
    description      = "springboot port from anywhere"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"  
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "ssh from anywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"   
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1" 
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_tls"
  }
}
resource "aws_iam_role_policy" "jeff-test_policy" {
  name = "jeff-test_policy"
  role = aws_iam_role.jeff_test_role.id
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "s3:GetObject",
        ]
        Effect   = "Allow"
        Resource = "${aws_s3_bucket.b.arn}/*",
      },
    ]
  })
}
resource "aws_iam_role" "jeff_test_role" {
  name = "jeff_test_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}
resource "aws_iam_instance_profile" "test_profile" {
  name = "jeff-test_profile"
  role = aws_iam_role.role.name
}
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}
resource "aws_iam_role" "role" {
  name               = "jeff-test_role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}
resource "aws_instance" "web" {
  ami           = "ami-02238ac43d6385ab3"
  instance_type = "t3.micro"
  key_name = "jeffs-key-pair"
  security_groups = [aws_security_group.allow_tls.name]
  iam_instance_profile = "jeff-test_profile"

  tags = {
    Name = "J-Hello"
  }
user_data  = <<EOF
#! /bin/bash
sudo yum update -y
sudo yum install -y java-17-amazon-corretto-headless
aws s3api get-object --bucket "jeff-bucket3" --key "g-hello-0.0.1-SNAPSHOT.jar" "g-hello-0.0.1-SNAPSHOT.jar"
java -jar g-hello-0.0.1-SNAPSHOT.jar
EOF
}
output "public_ipv4_dns" {
    description = "public IPv4 DNS of the EC2 instance"
    value       = aws_instance.web.public_dns
}

output "api_base_url" {
    description = "public IPv4 DNS of the EC2 instance"
    value       = "http://${aws_instance.web.public_dns}:8080/hello"
}